package peng;

import org.hibernate.HibernateException;
import peng.dao.DAO;
import peng.entity.Category;

public class CreateCategory extends DAO {
    public Category create(String title) throws Exception {
        try {
            begin();
            Category category = new Category(title);
            getSession().save(category);
            commit();
            return category;
        } catch (HibernateException e) {
            throw new Exception("Could not create category " + title, e);
        }
    }

    public static void main(String[] args) {
        CreateCategory self = new CreateCategory();
        String title = args[0];
        try {
            System.out.println("Creating category " + title);
            self.create(title);
            System.out.println("Created category");
            DAO.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }
}
