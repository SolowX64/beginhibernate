package peng;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import peng.dao.DAO;
import peng.entity.Advert;
import peng.entity.Category;

import java.util.List;
import java.util.Set;

public class ListAdverts extends DAO {
    public List<Category> list() throws Exception {
        try {
            begin();
            Query query = getSession().createQuery("from Category");
            List<Category> list = query.list();

            commit();
            return list;
        } catch (HibernateException e)
        {
            rollback();
            throw new Exception("Could not list the categories",e);
        }
    }

    public static void main(String[] args) {
        try {
            ListAdverts self = new ListAdverts();
            List<Category> categories = self.list();

            for(Category category : categories) {
                System.out.println("Category: " + category.getTitle());
                System.out.println();
                Set<Advert> adverts = category.getAdverts();

                for (Advert advert : adverts) {
                    System.out.println();
                    System.out.println("Title: " + advert.getTitle());
                    System.out.println(advert.getMessage());
                    System.out.println(" posted by " + advert.getUser().getName());
                }
            }
            DAO.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
