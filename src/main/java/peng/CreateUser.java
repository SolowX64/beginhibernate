package peng;

import org.hibernate.HibernateException;
import peng.dao.DAO;
import peng.entity.User;

public class CreateUser extends DAO {
    public User create(String username, String password) throws Exception {
        try {
            begin();
            User user = new User(username, password);
            getSession().save(user);
            commit();
            return user;
        } catch (HibernateException e){
            throw new Exception("Could not create user " + username, e);
        }
    }

    public static void main(String[] args) {
        String username = args[0];
        String password = args[1];

        try {
            CreateUser self = new CreateUser();
            System.out.println("Creating user " + username);
            self.create(username,password);
            System.out.println("Created user");
            DAO.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
